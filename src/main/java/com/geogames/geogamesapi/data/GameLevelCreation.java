package com.geogames.geogamesapi.data;

import io.swagger.annotations.ApiModelProperty;

public class GameLevelCreation {
    @ApiModelProperty(notes = "Ordinal number of the specific level.")
    Integer ord;
    @ApiModelProperty(notes = "Coordinates where the task will take place. Optionally leave an empty String.")
    String coordinates;
    @ApiModelProperty(notes = "The content of the task.")
    String description;
    @ApiModelProperty(notes = "An answer to the provided question/task. Letters will be converted to a lower case and white characters will be removed.")
    String answer;

    public GameLevelCreation() {

    }

    public GameLevelCreation(Integer ord, String coordinates, String description, String answer) {
        this.ord = ord;
        this.coordinates = coordinates;
        this.description = description;
        this.answer = answer;
    }

    public Integer getOrd() {
        return ord;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public String getDescription() {
        return description;
    }

    public String getAnswer() {
        return answer;
    }
}
