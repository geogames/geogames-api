package com.geogames.geogamesapi.data.repository;

import com.geogames.geogamesapi.data.entity.GameDetailsEntity;
import com.geogames.geogamesapi.util.GameType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GameDetailsRepository extends PagingAndSortingRepository<GameDetailsEntity, Integer> {
    List<GameDetailsEntity> findAllByGameType(GameType gameType, Pageable pageable);

    List<GameDetailsEntity> findAllByGameTypeAndTitleIgnoreCaseContaining(GameType gameType, String title, Pageable pageable);
}
