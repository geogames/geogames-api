package com.geogames.geogamesapi.data.repository;

import com.geogames.geogamesapi.data.entity.UserGamesEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserGamesRepository extends PagingAndSortingRepository<UserGamesEntity, Integer> {
}
