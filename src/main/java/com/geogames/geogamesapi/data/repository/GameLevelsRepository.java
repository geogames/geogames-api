package com.geogames.geogamesapi.data.repository;

import com.geogames.geogamesapi.data.entity.GameLevelsEntity;
import org.springframework.data.repository.CrudRepository;

public interface GameLevelsRepository extends CrudRepository<GameLevelsEntity, Integer> {
    Iterable<GameLevelsEntity> findAllByGameId(Integer gameId);
}
