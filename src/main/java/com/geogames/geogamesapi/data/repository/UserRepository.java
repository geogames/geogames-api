package com.geogames.geogamesapi.data.repository;

import com.geogames.geogamesapi.data.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByUsernameIgnoreCase(String username);
}
