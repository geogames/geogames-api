package com.geogames.geogamesapi.data.entity;

import com.geogames.geogamesapi.util.UserPrivilege;

import javax.persistence.*;

@Entity
@Table(name = "user_games")
public class UserGamesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userGamesSequence")
    @SequenceGenerator(name = "userGamesSequence", sequenceName = "user_games_sequence", allocationSize=1)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @Column(name = "user_privilege")
    @Enumerated(EnumType.STRING)
    private UserPrivilege userPrivilege;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private GameDetailsEntity gameDetails;

    public UserGamesEntity() {
    }

    public UserGamesEntity(Integer userId, UserPrivilege userPrivilege, GameDetailsEntity gameDetails) {
        this.userId = userId;
        this.userPrivilege = userPrivilege;
        this.gameDetails = gameDetails;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public UserPrivilege getUserPrivilege() {
        return userPrivilege;
    }

    public GameDetailsEntity getGameDetails() {
        return gameDetails;
    }
}
