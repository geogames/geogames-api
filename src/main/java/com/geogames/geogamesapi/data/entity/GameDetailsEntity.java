package com.geogames.geogamesapi.data.entity;

import com.geogames.geogamesapi.util.GameType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "game_details")
public class GameDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gameDetailsSequence")
    @SequenceGenerator(name = "gameDetailsSequence", sequenceName = "game_details_sequence", allocationSize=1)
    @Column(name = "game_id", unique = true, nullable = false)
    private Integer gameId;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "game_type")
    @Enumerated(EnumType.STRING)
    private GameType gameType;

    @Column(name = "date_created", nullable = false)
    private Timestamp dateCreated;

    @Column(name = "last_modified", nullable = false)
    private Timestamp lastModified;

    @OneToMany(mappedBy = "gameId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<GameLevelsEntity> gameLevels;

    @PrePersist
    void preInsert() {
        if (this.gameType == null) {
            this.gameType = GameType.PUBLIC;
        }
    }

    public GameDetailsEntity() {
    }

    public GameDetailsEntity(String title, String description, Timestamp dateCreated, Timestamp lastModified) {
        this.title = title;
        this.description = description;
        this.dateCreated = new Timestamp(dateCreated.getTime());
        this.lastModified = new Timestamp(lastModified.getTime());
    }

    public Integer getGameId() {
        return gameId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public GameType getGameType() {
        return gameType;
    }

    public Timestamp getDateCreated() {
        return new Timestamp(dateCreated.getTime());
    }

    public Timestamp getLastModified() {
        return new Timestamp(lastModified.getTime());
    }

    public List<GameLevelsEntity> getGameLevels() {
        return gameLevels;
    }
}
