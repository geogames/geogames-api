package com.geogames.geogamesapi.data.entity;

import com.geogames.geogamesapi.util.EncryptionType;
import com.geogames.geogamesapi.util.HashType;

import javax.persistence.*;

@Entity
@Table(name = "game_levels")
public class GameLevelsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gameLevelsSequence")
    @SequenceGenerator(name = "gameLevelsSequence", sequenceName = "game_levels_sequence", allocationSize=1)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "game_id", nullable = false)
    private Integer gameId;

    @Column(name = "ord", nullable = false)
    private Integer ord;

    @Column(name = "coordinates")
    private String coordiantes;

    @Column(name = "answer", nullable = false)
    private String answer;

    @Column(name = "hash_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private HashType hashType;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "encrypted_description")
    private String encryptedDescription;

    @Column(name = "encryption_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private EncryptionType encryptionType;

    public GameLevelsEntity() {
    }

    public GameLevelsEntity(Integer gameId, Integer ord, String coordiantes, String answer, HashType hashType, String description, String encryptedDescription, EncryptionType encryptionType) {
        this.gameId = gameId;
        this.ord = ord;
        this.coordiantes = coordiantes;
        this.answer = answer;
        this.hashType = hashType;
        this.description = description;
        this.encryptedDescription = encryptedDescription;
        this.encryptionType = encryptionType;
    }

    public Integer getId() {
        return id;
    }

    public Integer getGameId() {
        return gameId;
    }

    public Integer getOrd() {
        return ord;
    }

    public String getCoordiantes() {
        return coordiantes;
    }

    public String getAnswer() {
        return answer;
    }

    public HashType getHashType() {
        return hashType;
    }

    public String getEncryptedDescription() {
        return encryptedDescription;
    }

    public String getDescription() {
        return description;
    }

    public EncryptionType getEncryptionType() {
        return encryptionType;
    }
}
