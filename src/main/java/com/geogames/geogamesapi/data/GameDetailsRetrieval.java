package com.geogames.geogamesapi.data;

import com.geogames.geogamesapi.data.entity.GameDetailsEntity;
import com.geogames.geogamesapi.util.GameType;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

public class GameDetailsRetrieval {
    @ApiModelProperty(notes = "ID of the specific game. You can use it to retrieve tasks for the game.")
    private Integer gameId;
    @ApiModelProperty(notes = "Title of the game.")
    private String title;
    @ApiModelProperty(notes = "Description of the game.")
    private String description;
    @ApiModelProperty(notes = "Currently the only supported game type is: PUBLIC")
    private GameType gameType;
    @ApiModelProperty(notes = "Timestamp for when the game was originally created.")
    private Timestamp dateCreated;
    @ApiModelProperty(notes = "Not yet supported.")
    private String inviteUrl;

    public GameDetailsRetrieval(GameDetailsEntity gameDetailsEntity) {
        this.gameId = gameDetailsEntity.getGameId();
        this.title = gameDetailsEntity.getTitle();
        this.description = gameDetailsEntity.getDescription();
        this.gameType = gameDetailsEntity.getGameType();
        this.dateCreated = gameDetailsEntity.getDateCreated();
    }

    public Integer getGameId() {
        return gameId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public GameType getGameType() {
        return gameType;
    }

    public Timestamp getDateCreated() {
        return new Timestamp(dateCreated.getTime());
    }

    public String getInviteUrl() {
        return inviteUrl;
    }
}
