package com.geogames.geogamesapi.data;

import java.util.List;

public class GameRetrievalRequest {
    private GameDetailsRetrieval gameDetails;
    private List<GameLevelRetrieval> gameLevels;

    public GameRetrievalRequest(GameDetailsRetrieval gameDetails, List<GameLevelRetrieval> gameLevels) {
        this.gameDetails = gameDetails;
        this.gameLevels = gameLevels;
    }

    public GameDetailsRetrieval getGameDetails() {
        return gameDetails;
    }

    public List<GameLevelRetrieval> getGameLevels() {
        return gameLevels;
    }
}
