package com.geogames.geogamesapi.data;

import com.geogames.geogamesapi.data.entity.GameLevelsEntity;
import com.geogames.geogamesapi.util.EncryptionType;
import com.geogames.geogamesapi.util.HashType;
import io.swagger.annotations.ApiModelProperty;

public class GameLevelRetrieval {
    @ApiModelProperty(notes = "ID of the game this level belongs to.")
    private Integer gameId;
    @ApiModelProperty(notes = "Ordinal number of the specific level.")
    private Integer ord;
    @ApiModelProperty(notes = "Coordinates where the task will take place. Empty String means the coordinates were not provided.")
    private String coordinates;
    @ApiModelProperty(notes = "Indicates which hashing method should be used to calculate the answer's hash.")
    private HashType hashType;
    @ApiModelProperty(notes = "Description encrypted using a symmetric encryption algorithm for which the key is a hash of an answer to the previous task/question.")
    private String encryptedDescription;
    @ApiModelProperty(notes = "Indicates which encryption algorithm was used to encrypt the description.")
    private EncryptionType encryptionType;

    public GameLevelRetrieval(GameLevelsEntity gameLevelsEntity) {
        this.gameId = gameLevelsEntity.getGameId();
        this.ord = gameLevelsEntity.getOrd();
        this.coordinates = gameLevelsEntity.getCoordiantes();
        this.hashType = gameLevelsEntity.getHashType();
        this.encryptedDescription = gameLevelsEntity.getEncryptedDescription();
        this.encryptionType = gameLevelsEntity.getEncryptionType();
    }

    public Integer getGameId() {
        return gameId;
    }

    public Integer getOrd() {
        return ord;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public HashType getHashType() {
        return hashType;
    }

    public String getEncryptedDescription() {
        return encryptedDescription;
    }

    public EncryptionType getEncryptionType() {
        return encryptionType;
    }
}
