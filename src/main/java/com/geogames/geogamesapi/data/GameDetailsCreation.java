package com.geogames.geogamesapi.data;

import com.geogames.geogamesapi.util.GameType;
import io.swagger.annotations.ApiModelProperty;

public class GameDetailsCreation {
    @ApiModelProperty(notes = "Title of the game.")
    String title;
    @ApiModelProperty(notes = "Description of the game.")
    String description;
    @ApiModelProperty(notes = "Currently the only supported game type is: PUBLIC")
    GameType gameType;

    public GameDetailsCreation() {
    }

    public GameDetailsCreation(String title, String description, GameType gameType) {
        this.title = title;
        this.description = description;
        this.gameType = gameType;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public GameType getGameType() {
        return gameType;
    }
}
