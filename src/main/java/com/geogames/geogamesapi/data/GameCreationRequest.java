package com.geogames.geogamesapi.data;

import java.util.ArrayList;
import java.util.List;

public class GameCreationRequest {
    private GameDetailsCreation gameDetailsCreation;
    private List<GameLevelCreation> gameLevels = new ArrayList<>();

    public GameCreationRequest() {
    }

    public GameCreationRequest(GameDetailsCreation gameDetailsCreation, List<GameLevelCreation> gameLevels) {
        this.gameDetailsCreation = gameDetailsCreation;
        this.gameLevels = gameLevels;
    }

    public GameDetailsCreation getGameDetailsCreation() {
        return gameDetailsCreation;
    }

    public List<GameLevelCreation> getGameLevels() {
        return gameLevels;
    }
}
