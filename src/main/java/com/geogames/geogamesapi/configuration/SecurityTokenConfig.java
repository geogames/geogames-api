package com.geogames.geogamesapi.configuration;

import com.geogames.geogamesapi.web.JwtTokenAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {
    private ConfigurationProvider configurationProvider;
    private PublicKeyConfig publicKeyConfig;

    @Autowired
    public SecurityTokenConfig(ConfigurationProvider configurationProvider, PublicKeyConfig publicKeyConfig) {
        this.configurationProvider = configurationProvider;
        this.publicKeyConfig = publicKeyConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                // make sure we use stateless session; session won't be used to store user's state.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // handle an authorized attempts
                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                // Add a filter to validate the tokens with every request
                .addFilterAfter(new JwtTokenAuthenticationFilter(configurationProvider, publicKeyConfig), UsernamePasswordAuthenticationFilter.class)
                // authorization requests config
                .antMatcher(ConfigurationProvider.PRIVATE_URL_BASE+"/**")
                .authorizeRequests()
                // Any request must be authenticated
                .anyRequest().authenticated();
    }

    @Bean
    public HttpFirewall httpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowedHttpMethods(Arrays.asList("POST", "GET"));
        return firewall;
    }
}
