package com.geogames.geogamesapi.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationProvider {
    public static final String PRIVATE_URL_BASE = "/private";
    public static final String PUBLIC_URL_BASE = "/public";

    @Value("${security.jwt.header}")
    private String authenticationHeader;

    public String getAuthenticationHeader() {
        return authenticationHeader;
    }
}