package com.geogames.geogamesapi.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

@Component
public class PublicKeyConfig {

    @Value("${security.jwt.public.key.filepath}")
    private String publicKeyFilepath;

    private PublicKey publicKey;

    @PostConstruct
    private void init() throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(publicKeyFilepath));

        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        this.publicKey = kf.generatePublic(spec);
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }
}
