package com.geogames.geogamesapi.util;

import com.google.crypto.tink.Aead;
import com.google.crypto.tink.aead.AeadConfig;
import com.google.crypto.tink.subtle.AesGcmJce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

@Component
public class EncryptionUtility {

    private final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    private MessageDigest messageDigest;

    @PostConstruct
    public void init() throws GeneralSecurityException {
        AeadConfig.register();
//        SHA-256 produces a hash of size 256bits.
        messageDigest = MessageDigest.getInstance("SHA-256");
    }

    //    https://stackoverflow.com/questions/52171198/how-to-create-symmetric-encryption-key-with-google-tink
    @Autowired
    public EncryptionUtility() {
    }

//    Returns hex-encoded String.
    public String encryptMessageWithPassword(String message, String password) throws GeneralSecurityException {
        String normalizedPassword = normalizePassword(password);
        byte[] hashedPassword = messageDigest.digest(normalizedPassword.getBytes(StandardCharsets.UTF_8));

        Aead aead = new AesGcmJce(hashedPassword);

        byte[] encryptedMessage = aead.encrypt(message.getBytes(StandardCharsets.UTF_8), null);

        return bytesToHex(encryptedMessage);
    }

//    encryptedMessage should be a hex-encoded String.
    public String decryptMessageWithAPassword(String encryptedMessage, String password)
            throws GeneralSecurityException, UnsupportedEncodingException {
        String normalizedPassword = normalizePassword(password);
        byte[] hashedPassword = messageDigest.digest(normalizedPassword.getBytes(StandardCharsets.UTF_8));

        Aead aead = new AesGcmJce(hashedPassword);

        byte[] decryptedMessage = aead.decrypt(hexStringToByteArray(encryptedMessage), null);
        return new String(decryptedMessage, "UTF-8");
    }

    //    https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    //    https://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java/140861#140861
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private String normalizePassword(String password){
        return password.toLowerCase().replaceAll("\\s","");
    }
}