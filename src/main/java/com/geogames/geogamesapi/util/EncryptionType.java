package com.geogames.geogamesapi.util;

public enum EncryptionType {
    AES256_GCM,
    NONE
}
