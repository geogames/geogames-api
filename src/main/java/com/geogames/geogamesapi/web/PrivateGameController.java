package com.geogames.geogamesapi.web;

import com.geogames.geogamesapi.configuration.ConfigurationProvider;
import com.geogames.geogamesapi.data.GameCreationRequest;
import com.geogames.geogamesapi.service.GameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.GeneralSecurityException;
import java.security.Principal;

@RestController
@RequestMapping(ConfigurationProvider.PRIVATE_URL_BASE)
@Api(value = "privateGameController", description = "Operations available only for authenticated users. Every call has to provide an Authorization header.")
public class PrivateGameController {
    static final Logger LOGGER = LoggerFactory.getLogger(PrivateGameController.class);

    private GameService gameService;

    @Autowired
    public PrivateGameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Create a new GeoGame", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The game has been successfully registered."),
            @ApiResponse(code = 500, message = "An error has occurred while creating the game and the operation cannot be completed.")
    })
    public ResponseEntity createGame(@RequestBody GameCreationRequest gameCreationRequest,
                                     HttpServletRequest httpServletRequest) {
        Principal principal = httpServletRequest.getUserPrincipal();
        LOGGER.debug("Received a request to create a game from a user: {}", principal.getName());
        Integer gameId;
        try {
            gameId = gameService.createNewGame(principal, gameCreationRequest);
            LOGGER.debug("Created a new game with ID: {}", gameId);
        } catch (GeneralSecurityException ex) {
            return ResponseEntity
                    .status(500)
                    .build();
        }
        return ResponseEntity
                .ok(new JSONObject()
                        .put("status", "success")
                        .put("gameID", gameId)
                        .toString(2));
    }
}
