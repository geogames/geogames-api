package com.geogames.geogamesapi.web;

import com.geogames.geogamesapi.configuration.ConfigurationProvider;
import com.geogames.geogamesapi.data.GameDetailsRetrieval;
import com.geogames.geogamesapi.data.GameRetrievalRequest;
import com.geogames.geogamesapi.exception.NoSuchGameException;
import com.geogames.geogamesapi.service.GameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(ConfigurationProvider.PUBLIC_URL_BASE)
@Api(value = "publicGameController", description = "Publicly available endpoints for retrieving a list of public GeoGames and downloading a specific GeoGame.")
public class PublicGameController {
    static final Logger LOGGER = LoggerFactory.getLogger(PublicGameController.class);

    private GameService gameService;

    @Autowired
    public PublicGameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/games", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Get a list of public GeoGames")
    public List<GameDetailsRetrieval> getAllAvailableGames(@RequestParam("page") Optional<Integer> page,
                                                           @RequestParam("pageSize") Optional<Integer> pageSize,
                                                           @RequestParam("title") Optional<String> title) {
        LOGGER.debug("Received a request to list all available games with parameters: page={}, pageSize={}, gameName={}",
                page, pageSize, title);
        return gameService.getAllPublicGames(page, pageSize, title);
    }

    @RequestMapping(value = "/{gameid}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Get a complete GeoGame with a specified gameid.")
    public GameRetrievalRequest getSpecificGame(@PathVariable int gameid) {
        try {
            return gameService.getSpecificGame(gameid);
        } catch (NoSuchGameException ex) {
            return null;
        }
    }
}
