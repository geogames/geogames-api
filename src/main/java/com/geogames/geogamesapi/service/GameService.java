package com.geogames.geogamesapi.service;

import com.geogames.geogamesapi.data.*;
import com.geogames.geogamesapi.data.entity.GameDetailsEntity;
import com.geogames.geogamesapi.data.entity.GameLevelsEntity;
import com.geogames.geogamesapi.data.entity.UserEntity;
import com.geogames.geogamesapi.data.repository.GameDetailsRepository;
import com.geogames.geogamesapi.data.repository.GameLevelsRepository;
import com.geogames.geogamesapi.exception.NoSuchGameException;
import com.geogames.geogamesapi.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.GeneralSecurityException;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GameService {

    private UserService userService;
    private GameDetailsRepository gameDetailsRepository;
    private GameLevelsRepository gameLevelsRepository;
    private EncryptionUtility encryptionUtility;

    @Autowired
    public GameService(UserService userService, GameDetailsRepository gameDetailsRepository,
                       GameLevelsRepository gameLevelsRepository, EncryptionUtility encryptionUtility) {
        this.userService = userService;
        this.gameDetailsRepository = gameDetailsRepository;
        this.gameLevelsRepository = gameLevelsRepository;
        this.encryptionUtility = encryptionUtility;
    }

    @Transactional
    public Integer createNewGame(Principal principal, GameCreationRequest gameCreationRequest) throws GeneralSecurityException {
        UserEntity userEntity = userService.retrieveUserFromTheDatabase(principal);

        Timestamp timestampNow = new java.sql.Timestamp(System.currentTimeMillis());
        GameDetailsEntity gameDetailsEntity =
                new GameDetailsEntity(gameCreationRequest.getGameDetailsCreation().getTitle(),
                        gameCreationRequest.getGameDetailsCreation().getDescription(),
                        timestampNow,
                        timestampNow);

        gameDetailsEntity = persistGameDetails(gameDetailsEntity);

        userService.addGameToAUser(userEntity, UserPrivilege.OWNER, gameDetailsEntity);

        Integer newGameId = gameDetailsEntity.getGameId();
//        We do not want to encrypt the first level of the game.
        encryptAndPersistGameLevels(gameCreationRequest.getGameLevels(), newGameId);

        return newGameId;
    }

    protected GameDetailsEntity persistGameDetails(GameDetailsEntity gameDetailsEntity) {
        return gameDetailsRepository.save(gameDetailsEntity);
    }

    protected void encryptAndPersistGameLevels(List<GameLevelCreation> gameLevels, Integer gameId) throws GeneralSecurityException {
        gameLevels.sort(Comparator.comparing(GameLevelCreation::getOrd));
//        Do not encrypt the first level.
        GameLevelCreation firstGameLevelCreation = gameLevels.get(0);

        String previousAnswer = firstGameLevelCreation.getAnswer();

        GameLevelsEntity firstGameLevelEntity = new GameLevelsEntity(
                gameId,
                firstGameLevelCreation.getOrd(),
                firstGameLevelCreation.getCoordinates(),
                previousAnswer,
                HashType.SHA_256,
                firstGameLevelCreation.getDescription(),
                firstGameLevelCreation.getDescription(),
                EncryptionType.NONE);
        gameLevelsRepository.save(firstGameLevelEntity);

        for (int i = 1; i < gameLevels.size(); i++) {
            GameLevelCreation currentLevel = gameLevels.get(i);

            GameLevelsEntity gameLevelsEntity = new GameLevelsEntity(
                    gameId,
                    currentLevel.getOrd(),
                    currentLevel.getCoordinates(),
                    currentLevel.getAnswer(),
                    HashType.SHA_256,
                    currentLevel.getDescription(),
                    encryptionUtility.encryptMessageWithPassword(currentLevel.getDescription(), previousAnswer),
                    EncryptionType.AES256_GCM);
            gameLevelsRepository.save(gameLevelsEntity);

            previousAnswer = gameLevelsEntity.getAnswer();
        }
    }

    public List<GameDetailsRetrieval> getAllPublicGames(Optional<Integer> page, Optional<Integer> pageSize, Optional<String> title) {
        Pageable pageable = PageRequest.of(page.orElse(0), pageSize.orElse(10), Sort.Direction.DESC, "dateCreated");
        List<GameDetailsEntity> gameDetailsEntities;
        if (title.isPresent()) {
            gameDetailsEntities = gameDetailsRepository
                    .findAllByGameTypeAndTitleIgnoreCaseContaining(GameType.PUBLIC, title.get().trim().toLowerCase(), pageable);
        } else {
            gameDetailsEntities = gameDetailsRepository
                    .findAllByGameType(GameType.PUBLIC, pageable);
        }
        return gameDetailsEntities
                .stream()
                .map(gameDetailsEntity -> new GameDetailsRetrieval(gameDetailsEntity))
                .collect(Collectors.toList());
    }

    public GameRetrievalRequest getSpecificGame(Integer gameId) throws NoSuchGameException {
        GameDetailsEntity gameDetailsEntity = gameDetailsRepository
                .findById(gameId)
                .orElseThrow(() -> new NoSuchGameException(gameId));

        GameDetailsRetrieval gameDetailsRetrieval = new GameDetailsRetrieval(gameDetailsEntity);

        List<GameLevelRetrieval> gameLevels = gameDetailsEntity
                .getGameLevels()
                .stream()
                .map(gameLevelsEntity -> new GameLevelRetrieval(gameLevelsEntity))
                .collect(Collectors.toList());

        return new GameRetrievalRequest(gameDetailsRetrieval, gameLevels);
    }
}
