package com.geogames.geogamesapi.service;

import com.geogames.geogamesapi.data.entity.GameDetailsEntity;
import com.geogames.geogamesapi.data.entity.UserEntity;
import com.geogames.geogamesapi.data.entity.UserGamesEntity;
import com.geogames.geogamesapi.data.repository.UserGamesRepository;
import com.geogames.geogamesapi.data.repository.UserRepository;
import com.geogames.geogamesapi.util.UserPrivilege;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
public class UserService {

    private UserRepository userRepository;
    private UserGamesRepository userGamesRepository;

    @Autowired
    public UserService(UserRepository userRepository, UserGamesRepository userGamesRepository) {
        this.userRepository = userRepository;
        this.userGamesRepository = userGamesRepository;
    }

    @Transactional
    public UserEntity retrieveUserFromTheDatabase(Principal principal) {
        String username = principal.getName();
        UserEntity userEntity =
                userRepository
                        .findByUsernameIgnoreCase(username)
                        .orElseGet(() -> userRepository.save(new UserEntity(username)));

        return userEntity;
    }

    @Transactional
    public UserGamesEntity addGameToAUser(UserEntity userEntity, UserPrivilege userPrivilege, GameDetailsEntity gameDetailsEntity) {
        UserGamesEntity userGamesEntity = new UserGamesEntity(userEntity.getUserId(), userPrivilege, gameDetailsEntity);
        userGamesEntity = userGamesRepository.save(userGamesEntity);
        return userGamesEntity;
    }
}
