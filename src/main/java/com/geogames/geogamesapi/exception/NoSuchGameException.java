package com.geogames.geogamesapi.exception;

public class NoSuchGameException extends Exception {
    public NoSuchGameException(Integer gameId){
        super("Game with an ID: " + gameId + " doesn't exist or is private.");
    }
}
