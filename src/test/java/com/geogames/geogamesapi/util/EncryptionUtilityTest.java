package com.geogames.geogamesapi.util;

import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import static org.junit.Assert.assertEquals;

public class EncryptionUtilityTest {

    EncryptionUtility encryptionUtility;

    private static final String MESSAGE = "An example message. This message is very long and complex. Obviously contains sensitive information";
    private static final String PASSWORD = "veryStrongAndSuperSecretPassword";

    @Before
    public void setUp() throws Exception {
        encryptionUtility = new EncryptionUtility();
        encryptionUtility.init();
    }

    @Test
    public void givenMessageEncryptedWithPassword_whenDecryptingMessageWithSamePassword_sameMessageShouldBeReturned()
            throws GeneralSecurityException, UnsupportedEncodingException {
//        given
        String encryptedMessage = encryptionUtility.encryptMessageWithPassword(MESSAGE, PASSWORD);

//        when
        String decryptedMessage = encryptionUtility.decryptMessageWithAPassword(encryptedMessage, PASSWORD);

//        then
        assertEquals(MESSAGE, decryptedMessage);
    }
}