package com.geogames.geogamesapi.service;

import com.geogames.geogamesapi.data.entity.GameDetailsEntity;
import com.geogames.geogamesapi.data.entity.UserEntity;
import com.geogames.geogamesapi.data.entity.UserGamesEntity;
import com.geogames.geogamesapi.data.repository.UserGamesRepository;
import com.geogames.geogamesapi.data.repository.UserRepository;
import com.geogames.geogamesapi.util.UserPrivilege;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    private static final String USERNAME = "simpleUsername";

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserGamesRepository userGamesRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void givenExistingUser_whenRetrieveUserFromTheDatabaseIsCalled_returnUserEntity() {
//        given
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        UserDetails userDetails = new User(USERNAME, "Test1234", authorities);
        Principal userPrincipal = new AnonymousAuthenticationToken("notRelevant", userDetails, authorities);

        UserEntity userEntity = Mockito.mock(UserEntity.class);
        when(userRepository.findByUsernameIgnoreCase(eq(USERNAME))).thenReturn(Optional.of(userEntity));

//        when
        userService.retrieveUserFromTheDatabase(userPrincipal);

//        then
        verify(userRepository, times(1)).findByUsernameIgnoreCase(eq(USERNAME));
        verify(userRepository, never()).save(any(UserEntity.class));
    }

    @Test
    public void givenNonExistingUser_whenRetrieveUserFromTheDatabaseIsCalled_returnNewUserEntity() {
//        given
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        UserDetails userDetails = new User(USERNAME, "Test1234", authorities);
        Principal userPrincipal = new AnonymousAuthenticationToken("notRelevant", userDetails, authorities);

        UserEntity userEntity = Mockito.mock(UserEntity.class);
        when(userRepository.findByUsernameIgnoreCase(eq(USERNAME))).thenReturn(Optional.empty());
        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);

//        when
        userService.retrieveUserFromTheDatabase(userPrincipal);

//        then
        verify(userRepository, times(1)).findByUsernameIgnoreCase(eq(USERNAME));
        verify(userRepository, times(1)).save(any(UserEntity.class));
    }

    @Test
    public void givenValidInputData_whenAddGameToAUserIsCalled_userGameEntityIsSaved() {
//        given
        UserEntity userEntity = Mockito.mock(UserEntity.class);
        GameDetailsEntity gameDetailsEntity = Mockito.mock(GameDetailsEntity.class);

        when(userEntity.getUserId()).thenReturn(123);

//        when
        userService.addGameToAUser(userEntity, UserPrivilege.OWNER, gameDetailsEntity);

//        then
        verify(userGamesRepository,  times(1)).save(any(UserGamesEntity.class));
    }
}