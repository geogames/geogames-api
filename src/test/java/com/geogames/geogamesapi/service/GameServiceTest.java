package com.geogames.geogamesapi.service;

import com.geogames.geogamesapi.data.GameCreationRequest;
import com.geogames.geogamesapi.data.GameDetailsCreation;
import com.geogames.geogamesapi.data.GameLevelCreation;
import com.geogames.geogamesapi.data.entity.GameDetailsEntity;
import com.geogames.geogamesapi.data.entity.GameLevelsEntity;
import com.geogames.geogamesapi.data.entity.UserEntity;
import com.geogames.geogamesapi.data.repository.GameDetailsRepository;
import com.geogames.geogamesapi.data.repository.GameLevelsRepository;
import com.geogames.geogamesapi.exception.NoSuchGameException;
import com.geogames.geogamesapi.util.EncryptionType;
import com.geogames.geogamesapi.util.EncryptionUtility;
import com.geogames.geogamesapi.util.GameType;
import com.geogames.geogamesapi.util.HashType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.GeneralSecurityException;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    private static final String USERNAME = "simpleUsername";
    private static final Integer GAME_ID = new Integer(1944);

    @Mock
    UserService userService;

    @Mock
    GameDetailsRepository gameDetailsRepository;

    @Mock
    GameLevelsRepository gameLevelsRepository;

    @Spy
    private EncryptionUtility encryptionUtility;

    private GameService gameService;
    private Principal userPrincipal;

    @Before
    public void setUp() throws Exception {
        encryptionUtility = Mockito.spy(new EncryptionUtility());
        encryptionUtility.init();
        gameService = new GameService(userService, gameDetailsRepository, gameLevelsRepository, encryptionUtility);

        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        UserDetails userDetails = new User(USERNAME, "Test1234", authorities);
        userPrincipal = new AnonymousAuthenticationToken("notRelevant", userDetails, authorities);

        UserEntity userEntity = new UserEntity(USERNAME);

//        when
        when(userService.retrieveUserFromTheDatabase(eq(userPrincipal))).thenReturn(userEntity);
    }

    @Test
    public void givenExistingUser_whenCreateNewGameIsCalled_createAValidGame() throws GeneralSecurityException {
//        given
        GameDetailsCreation gameDetailsCreation =
                new GameDetailsCreation("catchingTitle", "greatDescription", GameType.PUBLIC);

        List<GameLevelCreation> gameLevels = new ArrayList<>();
        gameLevels.add(new GameLevelCreation(0, "", "trickyQuestion", "brilliantAnswer"));
        gameLevels.add(new GameLevelCreation(1, "", "veryHardQuestion", "geniusAnswer"));

        GameCreationRequest gameCreationRequest = new GameCreationRequest(gameDetailsCreation, gameLevels);

        GameDetailsEntity gameDetailsEntity = Mockito.mock(GameDetailsEntity.class);

        when(gameDetailsRepository.save(any(GameDetailsEntity.class))).thenReturn(gameDetailsEntity);
        when(gameDetailsEntity.getGameId()).thenReturn(GAME_ID);

//        when
        Integer result = gameService.createNewGame(userPrincipal, gameCreationRequest);

//        then
        verify(gameDetailsRepository, times(1)).save(any(GameDetailsEntity.class));
        verify(encryptionUtility, times(1)).encryptMessageWithPassword(eq("veryHardQuestion"), eq("brilliantAnswer"));
        verify(gameLevelsRepository, times(2)).save(any(GameLevelsEntity.class));
        assertEquals(GAME_ID, result);
    }

    @Test
    public void whenGetAllPublicGamesIsCalled_allPublicGamesShouldBeReturned() {
//        given
        Timestamp timestampNow = new java.sql.Timestamp(System.currentTimeMillis());
        GameDetailsEntity gameDetailsEntitySpy = Mockito.spy(
                new GameDetailsEntity("theBestGame", "GLHF", timestampNow, timestampNow));
        when(gameDetailsEntitySpy.getGameId()).thenReturn(GAME_ID);
        when(gameDetailsEntitySpy.getGameType()).thenReturn(GameType.PUBLIC);

        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "dateCreated");

        when(gameDetailsRepository.findAllByGameType(GameType.PUBLIC, pageable)).thenReturn(Arrays.asList(gameDetailsEntitySpy));

//        when
        gameService.getAllPublicGames(Optional.empty(), Optional.empty(), Optional.empty());

//        then
        verify(gameDetailsRepository, times(1)).findAllByGameType(eq(GameType.PUBLIC), eq(pageable));
    }

    @Test
    public void givenExistingGameId_whenGetSpecificGameIsCalled_theGameShouldBeReturned() throws NoSuchGameException {
//        given
        Timestamp timestampNow = new java.sql.Timestamp(System.currentTimeMillis());
        GameDetailsEntity gameDetailsEntitySpy = Mockito.spy(
                new GameDetailsEntity("theBestGame", "GLHF", timestampNow, timestampNow));
        when(gameDetailsEntitySpy.getGameId()).thenReturn(GAME_ID);
        when(gameDetailsEntitySpy.getGameType()).thenReturn(GameType.PUBLIC);

        GameLevelsEntity gameLevelsEntitySpy = Mockito.spy(
                new GameLevelsEntity(GAME_ID, 0, "", "brilliantAnswer", HashType.SHA_256,
                        "trickyQuestion", "trickyQuestion", EncryptionType.NONE)
        );

        when(gameDetailsEntitySpy.getGameLevels()).thenReturn(Arrays.asList(gameLevelsEntitySpy));
        when(gameDetailsRepository.findById(GAME_ID)).thenReturn(Optional.of(gameDetailsEntitySpy));

//        when
        gameService.getSpecificGame(GAME_ID);

//        then
        verify(gameDetailsRepository, times(1)).findById(eq(GAME_ID));
    }

    @Test(expected = NoSuchGameException.class)
    public void givenNonExistingGameId_whenGetSpecificGameIsCalled_exceptionIsThrown() throws NoSuchGameException {
//        given
        when(gameDetailsRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

//        when
        gameService.getSpecificGame(1994);
    }
}